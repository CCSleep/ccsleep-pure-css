<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <title>CSS Practice</title>
  <script type="text/javascript">

  window.addEventListener("load", function () {
    const loader = document.querySelector(".loader");
    loader.classList.add("hidden");
  });
  </script>
</head>

<body>

  <nav class="navbar">
    <div class="navbar__content">
        <a href="javascript:void(0);" class="toggle"><i class="fa fa-bars"></i></a>
    <a href="#" class="navbar__link" id="navbar__brand"><h3 href="#" class="navbar__brand">CCSleep</h3></a>
    <a class="navbar__link" data-scroll href="#home">Home</a>
    <a class="navbar__link" data-scroll href="#about">About</a>
    <a class="navbar__link" href="#">Contact</a>
    <div class="navbar__dropdown" href="#">
      <a href="javascript:void(0)" class="navbar__link dropdown">Dropdown</a>
      <div class="dropdown-content">
        <a href="#">Tarp</a>
        <a href="#">Smae</a>
        <a href="#">Jsut</a>
      </div>
    </div>

    <div class="align-right">
      <a href="#" class="navbar__icon"><i class="fab fa-facebook-f light-green-text-2"></i></a>
      <a href="#" class="navbar__icon"><i class="fab fa-discord light-green-text-2"></i></a>
    </div>


  </div>

  </nav>

  <div class="loader">
    <div class="loader__spinner"></div>
  </div>

  <header class="header__bg" id="home">
    <div class="header__main">
      <div class="header__title">
        CCSleep
      </div>
      <hr class="hr-long">
      <p class="header__text">
        An ordinary web developer
      </p>
    </div>

  </header>

  <header class="header" id="about">
    <div class="header__center">
      <div class="title-1">Introduction</div>
      <p class="text-muted">An introduction of myself</p>
    </div>
    <div class="header__wrapper">
      <div class="header__block">
        <div class="title-2">Who am I?</div>
        <p class="text-muted">My name is CCSleep. I am a web developer and my focus is backend developing. I would like to learn to improve myself everyday. My main language is PHP and Python.</p>
      </div>
    </div>

  </header>
  <footer class="footer">
    <div class="footer__copyright">
      &copy; 2020 Copyright&nbsp;<a href="#" class="footer__link">CCSleep</a>
    </div>
    <div class="align-right">
      <a href="#" class="footer__icon"><i class="fab fa-facebook-f light-green-text-2"></i></a>
      <a href="#" class="footer__icon"><i class="fab fa-discord light-green-text-2"></i></a>
    </div>
  </footer>

  <script src="https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@15.0/dist/smooth-scroll.polyfills.min.js"></script>

  <script src="js/main.js" charset="utf-8"></script>
</body>
</html>
