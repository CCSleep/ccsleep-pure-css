document.addEventListener("scroll", function(){
  var scrollLocation = document.querySelector("html").scrollTop;
  if (scrollLocation >= 50) {
    document.querySelector(".navbar").classList.add("scrolled");
  } else {
    document.querySelector(".navbar").classList.remove("scrolled");
  }
})

var sw = document.querySelector('.toggle')

sw.addEventListener("click", function() {
  var sa = document.querySelector(".navbar__content");
  if (sa.className == "navbar__content") {
    sa.className += " show";
  } else {
    sa.className = "navbar__content";
  }
});

var scroll = new SmoothScroll("a[href*='#']",{
  speed: 1000,
  speedAsDuration: true
});
